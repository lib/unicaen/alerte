Module UnicaenAlerte
====================

Ce module permet d'afficher des alertes sur une page de votre application, sous la forme de :
  - Toasts Bootstrap 5 :
    - Recommandé.
    - Positionnement fixe par défaut : en haut à droite ; empilés.
    - Cf. https://getbootstrap.com/docs/5.2/components/toasts/
  - Alerts Bootstrap 5 :
    - Affichage en place.
    - Cf. https://getbootstrap.com/docs/5.2/components/alerts/
  - Modals Bootstrap 5 :
    - En cas d'alertes multiples, un bouton permet de naviguer d'une alerte à l'autre.
    - Cf. https://getbootstrap.com/docs/5.2/components/modal/#toggle-between-modals
  - Alertes Javascript :
    - pas de HTML possible dans ce format évidemment.

Exemple au format Toasts Bootstrap 5 :
![unicaen-alerte.png](doc%2Funicaen-alerte.png)

Les alertes à afficher peuvent être :
  - puisées dans une base de données,
  - importées du FlashMessenger,
  - créées manuellement.

Une alerte a les caractéristiques principales suivantes :
  - `code` : Code unique de l'alerte (facultatif pour une alerte créée manuellement)
  - `text` : Texte de l'alerte
  - `title` : Titre/intitulé de l'alerte
  - `severity` : Sévérité de l'alerte ({@see Alerte::SEVERITY_INFO} par défaut)
  - `duration` : Durée d'apparition de l'alerte avant sa disparition, en ms (0 = durée infinie), SI SUPPORTÉE par le mode d'affichage choisi ultérieurement

Une alerte stockée en bdd peut avoir une période d'affichage (appelée de façon un peu ambitieuse "planning"), càd une date de début 
et une date de fin d'affichage, qui pourra être respectée ou non lors de la demande d'affichage.

Le module fournit l'aide de vue `alertes()` (cf. `UnicaenAlerte\View\Helper\AlerteViewHelper`).

Remarques :
- Le module ne propose pas d'IHM pour ajouter/modifier/supprimer des alertes en base de données.
- La prise en compte de la période d'affichage n'est possible que pour les alertes puisées en bdd.


Exemples d'utilisation
----------------------

### Affichage d'alertes ajoutées manuellement

```php
// Affichage d'une alerte créée manuellement, sous forme de Modal Bootstrap : 
echo $this->alertes()->addAlerteNew("Quelque chose s'est mal passé !!", "Votre attention, svp !", 'danger')->renderAsBootstrapModals();
// Affichage d'une alerte créée manuellement, sous forme de Toast Bootstrap disparaissant au bout de 3 secondes : 
echo $this->alertes()addAlerteNew("Youpi !!", "Félicitations", 'success', 3000);->renderAsBootstrapToasts();
```

### Affichage d'alertes puisées dans la bdd

```php
// Affichage de toutes les alertes puisées en bdd, sous forme de Toasts Bootstrap, à condition que leur planning d'affichage inclue la date du jour :
echo $this->alertes()->addAlertesFromDatabase(true)->renderAsBootstrapToasts();
// Idem, sous forme d'Alerts Bootstrap classiques :
echo $this->alertes()->addAlertesFromDatabase(true)->renderAsBootstrapAlerts();
// Idem, sous forme de Modals Bootstrap chaînées :
echo $this->alertes()->addAlertesFromDatabase(true)->renderAsBootstrapModals();
// Idem, sous forme d'alertes Javascript :
echo $this->alertes()->addAlertesFromDatabase(true)->renderAsJavascriptAlerts();
// Affichage d'une alerte puisée en bdd identifiée par son code, sous forme d'un Toast Bootstrap, à condition que son planning d'affichage inclue la date du jour :
echo $this->alertes()->addAlerteFromDatabase('ALERTE_MAINTENANCE', true)->renderAsBootstrapToasts();
// Affichage d'une alerte puisée en bdd identifiée par son code, sous forme d'un Toast Bootstrap, quelque soit son planning d'affichage :
echo $this->alertes()->addAlerteFromDatabase('ALERTE_FERMETURE_ESTIVALE')->renderAsBootstrapToasts();
```

### Affichage d'alertes issues du FlashMessenger

```php
echo $this->alertes()->addAlertesFromFlashMessenger()->renderAsBootstrapToasts();
```

### Affichage aggrégé

```php
echo $this->alertes()
    ->addAlerteNew("Quelque chose s'est mal passé !!", "Votre attention, svp !", 'danger')
    ->addAlerteNew("Youpi !!", "Félicitations", 'success', 3000) // disparition automatique au bout de 3s
    ->addAlerteFromDatabase('ALERTE_MAINTENANCE')
    ->addAlerteFromDatabase('ALERTE_FERMETURE_ESTIVALE', false) // pas de respect de la période d'affichage
    ->addAlertesFromFlashMessenger()
    ->renderAsBootstrapToasts();
```

### Usage avancé

```php
/** @var \UnicaenAlerte\View\Helper\AlerteViewHelper $alertesHelper */
$alertesHelper = $this->alertes();
$alertesHelper->getFlashMessengerContainer()
    ->setSeverityDuration('success', 0); // pas de disparition automatique
$alertesHelper->getArrayContainer()
    ->addAlerteNew("Quelque chose s'est mal passé !!", "Votre attention, svp !", 'danger')
    ->addAlerteNew("Youpi !!", "Félicitations", 'success', 3000);
$alertesHelper->getDatabaseContainer()
    ->withCodeAndMatchingPlanning('ALERTE_MAINTENANCE')
    ->withCodeAndMatchingPlanning('ALERTE_FERMETURE_ESTIVALE', false);
echo $alertesHelper
    ->addAlertesFromDatabase()
    ->addAlertesFromFlashMessenger()
    ->renderAsBootstrapToasts();
```


Installation
------------

### PHP

```bash
composer require unicaen/alerte
```

N'oubliez pas d'activer le module dans `config/application.config.php`.


### Base de données

Cf. [Script SQL de création des objets](./data/sql/schema.pgsql.sql)
