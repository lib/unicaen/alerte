<?php

namespace UnicaenAlerte;

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use UnicaenAlerte\Container\Database\DatabaseContainer;
use UnicaenAlerte\Container\Database\DatabaseContainerFactory;
use UnicaenAlerte\Container\FlashMessenger\FlashMessengerContainer;
use UnicaenAlerte\Container\FlashMessenger\FlashMessengerContainerFactory;
use UnicaenAlerte\Entity\Db\Repository\AlerteRepository;
use UnicaenAlerte\Entity\Db\Repository\AlerteRepositoryFactory;
use UnicaenAlerte\Form\AlerteForm;
use UnicaenAlerte\Form\AlerteFormFactory;
use UnicaenAlerte\Hydrator\AlerteHydrator;
use UnicaenAlerte\Hydrator\AlerteHydratorFactory;
use UnicaenAlerte\Service\Db\AlerteService;
use UnicaenAlerte\Service\Db\AlerteServiceFactory;
use UnicaenAlerte\View\Helper\AlerteViewHelper;
use UnicaenAlerte\View\Helper\AlerteViewHelperFactory;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenAlerte\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAlerte/Entity/Db/Mapping',
                ],
            ],
        ],
    ],
    'bjyauthorize'    => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'UnicaenAlerte' => [],
            ],
        ],
        'guards' => [

        ],
    ],
    'router' => [
        'routes' => [
            'unicaen-avis' => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/unicaen-avis',
                    'defaults'      => [
//                        'controller' => RapportActiviteController::class,
                    ],
                ],
                'may_terminate' => false,
                'child_routes'  => [

                ],
            ],
        ],
    ],
    
    'service_manager' => [
        'factories' => [
            AlerteService::class => AlerteServiceFactory::class,
            AlerteRepository::class => AlerteRepositoryFactory::class,

            DatabaseContainer::class => DatabaseContainerFactory::class,
            FlashMessengerContainer::class => FlashMessengerContainerFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [

        ],
    ],
    'controller_plugins' => [
        'factories' => [

        ],
    ],
    'hydrators' => [
        'factories' => [
            AlerteHydrator::class => AlerteHydratorFactory::class,
        ],
    ],
    'form_elements'   => [
        'factories' => [
            AlerteForm::class => AlerteFormFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            AlerteViewHelper::class => AlerteViewHelperFactory::class,
        ],
        'aliases' => [
            'alertes' => AlerteViewHelper::class,
        ],
        'shared' => [
            AlerteViewHelper::class => false,
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];