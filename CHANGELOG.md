CHANGELOG
=========

3.1.1
-----
- Suppression de la dépendance avec laminas/laminas-dependency-plugin.

3.1.0
-----
- Possibilité d'inclure les messages du FlashMessenger dans les alertes affichées.

3.0.0
-----
- Refonte pour permettre la création manuelle d'alertes (incompatible avec version 2)
- Correction de la navigation entre les modales.
- Amélioration de la documentation.

2.0.0
-----
- Corrections pour passage à PHP 8

1.1.2
-----
- [FIX] Module.php : activation redondante du ModuleRouteListener (à faire 1 fois uniquement dans le module Application)

1.1.1
-----
- Retrait de la navigation et du routing 

1.1.0
-----
- Ajout de spécificateurs de sélection d'alerte
- Nouveau format : Modal Bootstrap
- Amélioration des formats Toast et Alert Bootstrap

1.0.0
-----
- Première version
