<?php

namespace UnicaenAlerte\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use UnicaenAlerte\Container\Array\ArrayContainer;
use UnicaenAlerte\Container\ContainerInterface;
use UnicaenAlerte\Container\Database\DatabaseContainer;
use UnicaenAlerte\Container\FlashMessenger\FlashMessengerContainer;
use UnicaenAlerte\Entity\Db\Alerte;

/**
 * Aide de vue permettant d'afficher des alertes sous l'une des formes suivantes :
 *   - Toasts Bootstrap 5
 *   - Alerts Bootstrap 5
 *   - Modals Bootstrap 5
 *   - Alertes Javascript
 *
 * @property \Laminas\View\Renderer\PhpRenderer $view
 */
class AlerteViewHelper extends AbstractHelper
{
    /** @var \UnicaenAlerte\Container\ContainerInterface[] */
    private array $containers = [];

    private ArrayContainer $arrayContainer;
    private DatabaseContainer $databaseContainer;
    private FlashMessengerContainer $flashMessengerContainer;

    protected string $partial = 'unicaen-alerte/partial/as-bs-alerts';
    protected ?array $alertes = null;

    public function __construct()
    {
        $this->arrayContainer = new ArrayContainer();
        $this->addContainer($this->arrayContainer);
    }

    public function setDatabaseContainer(DatabaseContainer $databaseContainer): void
    {
        $this->databaseContainer = $databaseContainer;
    }

    public function setFlashMessengerContainer(FlashMessengerContainer $flashMessengerContainer): void
    {
        $this->flashMessengerContainer = $flashMessengerContainer;
    }

    public function __invoke(): self
    {
        return $this;
    }

    /**
     * Génère de HTML d'affichage des éventuelles alertes.
     */
    public function __toString(): string
    {
        $this->loadAlertes();

        return $this->renderPartial($this->partial);
    }

    public function addContainer(ContainerInterface $container): self
    {
        $this->containers[get_class($container)] = $container;
        return $this;
    }

    protected function loadAlertes(): void
    {
        $this->alertes = [];
        foreach ($this->containers as $container) {
            $this->alertes = array_merge($this->alertes, $container->fetchAlertes());
        }

        $this->sortAlertes();
    }

    protected function sortAlertes(): void
    {
        usort($this->alertes, Alerte::sorter());
    }

    /**
     * Affiche les éventuelles alertes sous forme de Toasts Bootstrap
     * (https://getbootstrap.com/docs/5.2/components/toasts/).
     */
    public function renderAsBootstrapToasts(): self
    {
        $this->partial = 'unicaen-alerte/partial/as-bs-toasts';
        return $this;
    }

    /**
     * Affiche les éventuelles alertes sous forme d'Alerts Bootstrap
     * (https://getbootstrap.com/docs/5.2/components/alerts/).
     */
    public function renderAsBootstrapAlerts(): self
    {
        $this->partial = 'unicaen-alerte/partial/as-bs-alerts';
        return $this;
    }

    /**
     * Affiche les éventuelles alertes sous forme de Modals Bootstrap chaînées
     * (https://getbootstrap.com/docs/5.2/components/modal/#toggle-between-modals).
     */
    public function renderAsBootstrapModals(): self
    {
        $this->partial = 'unicaen-alerte/partial/as-bs-modals';
        return $this;
    }

    /**
     * Affiche les éventuelles alertes sous forme d'alertes Javascript.
     */
    public function renderAsJavascriptAlerts(): self
    {
        $this->partial = 'unicaen-alerte/partial/as-js-alerts';
        return $this;
    }

    /**
     * Délègue l'affichage des alertes au partial spécifié.
     */
    public function renderAsPartial(string $partial): self
    {
        $this->partial = $partial;
        return $this;
    }

    protected function renderPartial(string $partial): string
    {
        if (empty($this->alertes)) {
            return '';
        }

        /** @var \Laminas\View\Helper\Partial $partialHelper */
        $partialHelper = $this->view->plugin('partial');

        return $partialHelper($partial, ['alertes' => $this->alertes]);
    }

    public function getArrayContainer(): ArrayContainer
    {
        return $this->arrayContainer;
    }

    public function getDatabaseContainer(): DatabaseContainer
    {
        return $this->databaseContainer;
    }

    public function getFlashMessengerContainer(): FlashMessengerContainer
    {
        return $this->flashMessengerContainer;
    }


    ///////////////////////////////////////////// shortcuts helper methods ////////////////////////////////////////////

    ///////// array container /////////

    /**
     * Ajoute une alerte déjà instanciée.
     */
    public function addAlerte(Alerte $alerte): static
    {
        $this->arrayContainer->addAlerte($alerte);
        return $this;
    }

    /**
     * Ajoute une alerte créée à partir des attributs spécifiés.
     *
     * @param string $text Texte de l'alerte
     * @param string $title Titre/intitulé de l'alerte
     * @param string $severity Sévérité de l'alerte, ex : {@see Alerte::SEVERITY_DANGER}
     * @param int $duration Durée d'apparition de l'alerte avant sa disparition, en ms (0 = durée infinie), SI SUPPORTÉE
     * par le mode d'affichage choisi ultérieurement
     */
    public function addAlerteNew(string $text, string $title, string $severity = Alerte::SEVERITY_INFO, int $duration = 0): static
    {
        $this->arrayContainer->addAlerteNew($text, $title, $severity, $duration);
        return $this;
    }

    ///////// database container /////////

    /**
     * Ajoute une alerte puisée en bdd, en respectant ou non son planning d'affichage.
     *
     * @param string $code Code unique de l'alerte en bdd
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     * @return $this
     */
    public function addAlerteFromDatabase(string $code, bool $matchingPlanning = true): static
    {
        $this->databaseContainer->withCodeAndMatchingPlanning($code, $matchingPlanning);
        $this->addContainer($this->databaseContainer);
        return $this;
    }

    /**
     * Ajoute toutes les alertes puisées en bdd, en respectant ou non leur planning d'affichage.
     *
     * @param bool $withMatchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     * @return $this
     */
    public function addAlertesFromDatabase(bool $withMatchingPlanning = true): static
    {
        $this->databaseContainer->withMatchingPlanning($withMatchingPlanning);
        $this->addContainer($this->databaseContainer);
        return $this;
    }

    //////// flash messenger container //////////

    /**
     * Ajoute toutes les alertes puisées en bdd, en respectant ou non leur planning d'affichage.
     *
     * @return $this
     */
    public function addAlertesFromFlashMessenger(bool $currentMessages = false, string $namespace = null): static
    {
        $this->flashMessengerContainer->addImport($currentMessages, $namespace);
        $this->addContainer($this->flashMessengerContainer);
        return $this;
    }
}