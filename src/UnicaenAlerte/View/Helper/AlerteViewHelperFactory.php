<?php

namespace UnicaenAlerte\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenAlerte\Container\Database\DatabaseContainer;
use UnicaenAlerte\Container\FlashMessenger\FlashMessengerContainer;

class AlerteViewHelperFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AlerteViewHelper
    {
        $helper = new AlerteViewHelper();

        /** @var DatabaseContainer $databaseContainer */
        $databaseContainer = $container->get(DatabaseContainer::class);
        $helper->setDatabaseContainer($databaseContainer);

        /** @var FlashMessengerContainer $flashMessengerContainer */
        $flashMessengerContainer = $container->get(FlashMessengerContainer::class);
        $helper->setFlashMessengerContainer($flashMessengerContainer);

        return $helper;
    }

}