<?php

namespace UnicaenAlerte\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Alerte
{
    const SEVERITY_SUCCESS = 'success';
    const SEVERITY_INFO = 'info';
    const SEVERITY_WARNING = 'warning';
    const SEVERITY_DANGER = 'danger';

    const SEVERITIES = [
        self::SEVERITY_SUCCESS,
        self::SEVERITY_INFO,
        self::SEVERITY_WARNING,
        self::SEVERITY_DANGER,
    ];

    const SEVERITIES_ORDERS_DEFAULTS = [
        self::SEVERITY_SUCCESS => 1,
        self::SEVERITY_DANGER => 2,
        self::SEVERITY_WARNING => 3,
        self::SEVERITY_INFO => 4,
    ];

    private int $id;
    private string $code;
    private string $title;
    private string $text;
    private string $severity;
    private int $duration = 0;
    private bool $dismissible = true;

    /**
     * @var \UnicaenAlerte\Entity\Db\AlertePlanning[]|\Doctrine\Common\Collections\Collection
     */
    private Collection $plannings;

    /**
     * Instancie une alerte ayant les propriétés spécifiées.
     *
     * @param string $code Code unique de l'alerte
     * @param string $text Texte de l'alerte
     * @param string $title Titre/intitulé de l'alerte
     * @param string $severity Sévérité de l'alerte ({@see Alerte::SEVERITY_INFO} par défaut)
     * @param int $duration Durée d'apparition de l'alerte avant sa disparition, en ms (0 = durée infinie), SI SUPPORTÉE
     * par le mode d'affichage choisi ultérieurement
     *
     * @return \UnicaenAlerte\Entity\Db\Alerte
     */
    static public function create(string $code,
                                  string $text,
                                  string $title,
                                  string $severity = Alerte::SEVERITY_INFO,
                                  int    $duration = 0): static
    {
        $alerte = new static();
        $alerte
            ->setCode($code)
            ->setText($text)
            ->setTitle($title)
            ->setSeverity($severity)
            ->setDuration($duration);

        return $alerte;
    }

    static public function sorter(?array $severitiesOrders = null): callable
    {
        if (!$severitiesOrders) {
            $severitiesOrders = static::SEVERITIES_ORDERS_DEFAULTS;
        }
        return fn(Alerte $a1, Alerte $a2) =>
            ($severitiesOrders[$a1->getSeverity()] ?? 100) <=> ($severitiesOrders[$a2->getSeverity()] ?? 100);
    }

    public function __construct()
    {
        $this->plannings = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getCode();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return self
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeverity(): string
    {
        return $this->severity;
    }

    /**
     * @param string $severity
     * @return self
     */
    public function setSeverity(string $severity): self
    {
        $this->severity = $severity;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return self
     */
    public function setDuration(int $duration): self
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDismissible(): bool
    {
        return $this->dismissible;
    }

    /**
     * @param bool $dismissible
     * @return self
     */
    public function setDismissible(bool $dismissible = true): self
    {
        $this->dismissible = $dismissible;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|\UnicaenAlerte\Entity\Db\AlertePlanning[]
     */
    public function getPlannings(): Collection
    {
        return $this->plannings;
    }

    public function addPlannings(iterable $plannings): self
    {
        foreach ($plannings as $avisComplem)
            $this->plannings->add($avisComplem);

        return $this;
    }

    public function removePlannings(iterable $plannings): self
    {
        foreach ($plannings as $avisComplem)
            $this->plannings->removeElement($avisComplem);

        return $this;
    }
}
