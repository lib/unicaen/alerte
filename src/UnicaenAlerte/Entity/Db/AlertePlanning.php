<?php

namespace UnicaenAlerte\Entity\Db;

use DateTime;

class AlertePlanning
{
    private int $id;
    private DateTime $startDate;
    private DateTime $endDate;
    private ?string $severity = null;

    /**
     * @var \UnicaenAlerte\Entity\Db\Alerte
     */
    private Alerte $alerte;

    /**
     * Méthode de tri selon l'attribut 'debut'.
     *
     * @param \UnicaenAlerte\Entity\Db\AlertePlanning $ac1
     * @param \UnicaenAlerte\Entity\Db\AlertePlanning $ac2
     * @return int
     */
    static public function sorterByStartDate(AlertePlanning $ac1, AlertePlanning $ac2): int
    {
        return $ac1->getStartDate()->getTimestamp() <=> $ac2->getStartDate()->getTimestamp();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(DateTime $startDate): self
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(DateTime $endDate): self
    {
        $this->endDate = $endDate;
        return $this;
    }

    public function getSeverity(): ?string
    {
        return $this->severity;
    }

    public function setSeverity(string $severity): self
    {
        $this->severity = $severity;
        return $this;
    }

    public function setAlerte(Alerte $alerte): self
    {
        $this->alerte = $alerte;

        return $this;
    }

    public function getAlerte(): Alerte
    {
        return $this->alerte;
    }
}
