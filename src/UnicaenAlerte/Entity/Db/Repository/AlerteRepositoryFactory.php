<?php

namespace UnicaenAlerte\Entity\Db\Repository;

use Psr\Container\ContainerInterface;
use UnicaenAlerte\Entity\Db\Alerte;

class AlerteRepositoryFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AlerteRepository
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        return new AlerteRepository($em, $em->getClassMetadata(Alerte::class));
    }
}