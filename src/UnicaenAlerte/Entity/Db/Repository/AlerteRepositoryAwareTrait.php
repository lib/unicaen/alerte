<?php

namespace UnicaenAlerte\Entity\Db\Repository;

trait AlerteRepositoryAwareTrait
{
    protected AlerteRepository $alerteRepository;

    /**
     * @param \UnicaenAlerte\Entity\Db\Repository\AlerteRepository $alerteRepository
     */
    public function setAlerteRepository(AlerteRepository $alerteRepository): void
    {
        $this->alerteRepository = $alerteRepository;
    }
}