<?php

namespace UnicaenAlerte\Container;

/**
 * Spécification minimale d'un container d'alertes supportant le filtrage par code et par période d'affichage.
 */
interface FilterableContainerInterface extends ContainerInterface
{
    /**
     * Sélection précise des alertes par leur code.
     *
     * @param string[] $codes
     */
    public function withCodes(array $codes): static;

    /**
     * Sélection précise des alertes par leur code,
     * en précisant si les alertes doivent avoir un planning qui inclue la date du jour.
     *
     * @param string[] $codes
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     */
    public function withCodesAndMatchingPlanning(array $codes, bool $matchingPlanning = true): static;

    /**
     * Sélection précise d'une alerte par son code.
     *
     * @param string $code
     */
    public function withCode(string $code): static;

    /**
     * Sélection précise d'une alerte par son code,
     * en précisant si l'alerte doit avoir un planning qui inclue la date du jour.
     *
     * @param string $code
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     */
    public function withCodeAndMatchingPlanning(string $code, bool $matchingPlanning = true): static;

    /**
     * Permet de ne sélectionner que les alertes ayant un planning qui inclue la date du jour.
     *
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     */
    public function withMatchingPlanning(bool $matchingPlanning = true): static;
}