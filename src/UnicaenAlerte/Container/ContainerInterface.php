<?php

namespace UnicaenAlerte\Container;

/**
 * Spécification minimale d'un container d'alertes.
 */
interface ContainerInterface
{
    /**
     * Fetch des alertes à afficher.
     *
     * @return \UnicaenAlerte\Entity\Db\Alerte[]
     */
    public function fetchAlertes(): array;
}