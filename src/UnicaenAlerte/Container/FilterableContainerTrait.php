<?php

namespace UnicaenAlerte\Container;

use UnicaenAlerte\Entity\Db\Alerte;

/**
 * Mécanisme de réutilisation du code implémentant le filtrage d'alertes par code et par période d'affichage.
 *
 * @property null|Alerte[] $alertes
 */
trait FilterableContainerTrait
{
    /**
     * Sélection précise des alertes par leur code,
     * en précisant pour chacune si elle doit avoir un planning qui inclue la date du jour pour être retenue.
     *
     * @var bool[] $code => $withMatchingPlanning
     */
    protected array $withCodesAndMatchingPlanning = [];

    /**
     * Spécifie *globalement* si les alertes doivent avoir un planning qui inclue la date du jour pour être retenues.
     *
     * @var bool
     */
    protected bool $withMatchingPlanning = false;

    /**
     * Sélection précise des alertes par leur code.
     *
     * @param string[] $codes Codes uniques des alertes
     */
    public function withCodes(array $codes): static
    {
        return $this->withCodesAndMatchingPlanning($codes, false);
    }

    /**
     * Sélection précise des alertes par leur code,
     * en précisant si les alertes doivent avoir un planning qui inclue la date du jour.
     *
     * @param string[] $codes Codes uniques des alertes
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     * @return static
     */
    public function withCodesAndMatchingPlanning(array $codes, bool $matchingPlanning = true): static
    {
        foreach ($codes as $code) {
            $this->withCodeAndMatchingPlanning($code, $matchingPlanning);
        }

        return $this;
    }

    /**
     * Sélection précise d'une alerte par son code.
     *
     * @param string $code Code unique de l'alerte
     * @return static
     */
    public function withCode(string $code): static
    {
        return $this->withCodeAndMatchingPlanning($code, false);
    }

    /**
     * Sélection précise d'une alerte par son code,
     * en précisant si l'alerte doit avoir un planning qui inclue la date du jour.
     *
     * @param string $code Code unique de l'alerte
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     * @return static
     */
    public function withCodeAndMatchingPlanning(string $code, bool $matchingPlanning = true): static
    {
        $this->withCodesAndMatchingPlanning[$code] = $matchingPlanning;
        $this->alertes = null;

        $this->withMatchingPlanning = false;

        return $this;
    }

    /**
     * Permet de ne sélectionner que les alertes ayant un planning qui inclue la date du jour.
     *
     * @param bool $matchingPlanning L'alerte doit-elle avoir un planning qui inclue la date du jour pour être retenue ?
     * @return static
     */
    public function withMatchingPlanning(bool $matchingPlanning = true): static
    {
        $this->withMatchingPlanning = $matchingPlanning;
        $this->alertes = null;

        return $this;
    }
}