<?php

namespace UnicaenAlerte\Container\Array;

use UnicaenAlerte\Container\ContainerInterface;
use UnicaenAlerte\Entity\Db\Alerte;

/**
 * Container basique de type tableau permettant d'ajouter manuellement des alertes.
 */
class ArrayContainer implements ContainerInterface
{
    /**
     * Alertes à afficher.
     * @var Alerte[]
     */
    protected array $alertes = [];

    public function addAlerte(Alerte $alerte): self
    {
        $this->alertes[] = $alerte;

        return $this;
    }

    public function addAlerteNew(string $text,
                                 string $title,
                                 string $severity = Alerte::SEVERITY_INFO,
                                 int    $duration = 0): self
    {
        $alerte = Alerte::create(uniqid('ALERTE_'), $text, $title, $severity, $duration);

        return $this->addAlerte($alerte);
    }

    /**
     * @return array|\UnicaenAlerte\Entity\Db\Alerte[]
     */
    public function fetchAlertes(): array
    {
        return $this->alertes;
    }
}