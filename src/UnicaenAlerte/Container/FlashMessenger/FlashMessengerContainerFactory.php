<?php

namespace UnicaenAlerte\Container\FlashMessenger;

use Laminas\Mvc\Controller\PluginManager;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger as FlashMessengerPlugin;
use Psr\Container\ContainerInterface;

class FlashMessengerContainerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): FlashMessengerContainer
    {
        $service = new FlashMessengerContainer();

        /** @var FlashMessengerPlugin $flashMessengerPlugin */
        $flashMessengerPlugin = $container->get(PluginManager::class)->get('flashMessenger');
        $service->setFlashMessengerPlugin($flashMessengerPlugin);

        return $service;
    }

}