<?php

namespace UnicaenAlerte\Container\Database;

use Doctrine\ORM\Query\Expr\Join;
use UnicaenAlerte\Container\FilterableContainerInterface;
use UnicaenAlerte\Container\FilterableContainerTrait;
use UnicaenAlerte\Entity\Db\Alerte;
use UnicaenAlerte\Entity\Db\Repository\AlerteRepositoryAwareTrait;

/**
 * Container puisant les alertes dans une base de données.
 */
class DatabaseContainer implements FilterableContainerInterface
{
    use FilterableContainerTrait;
    use AlerteRepositoryAwareTrait;

    public function fetchAlertes(): array
    {
        $qb = $this->alerteRepository->createQueryBuilder('a')
            ->addSelect('p') // seuls les plannings (éventuels) incluant la date du jour nous intéressent
            ->leftJoin('a.plannings', 'p', Join::WITH, 'current_timestamp() between p.startDate and p.endDate');

        $ors = [];
        foreach ($this->withCodesAndMatchingPlanning as $code => $matchingPlanning) {
            // le code de l'alerte doit matcher
            $param = uniqid(':code_');
            $expr = "a.code = $param";
            $qb->setParameter($param, $code);
            if ($matchingPlanning) {
                // et en plus, un planning doit exister
                $expr = $qb->expr()->andX($expr, 'p.id is not null');
            }
            $ors[] = $expr;
        }
        if ($ors) {
            $qb->andWhere($qb->expr()->orX(...$ors));
        }

        // filtre global portant sur toutes les alertes.
        if ($this->withMatchingPlanning) {
            $qb->andWhere('p.id is not null');
        }

        /** @var Alerte[] $alertes */
        $alertes = $qb->getQuery()->getResult();

        // la sévérité spécifiée dans l'eventuel planning écrase la sévérité par défaut :
        foreach ($alertes as $alerte) {
            /** @var \UnicaenAlerte\Entity\Db\AlertePlanning $planning */
            $planning = $alerte->getPlannings()->first(); // un seul planning en cours pris en compte
            if ($planning && $severity = $planning->getSeverity()) {
                $alerte->setSeverity($severity);
                break;
            }
        }

        return $alertes;
    }
}