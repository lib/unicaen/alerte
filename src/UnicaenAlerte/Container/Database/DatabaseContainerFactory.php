<?php

namespace UnicaenAlerte\Container\Database;

use Psr\Container\ContainerInterface;
use UnicaenAlerte\Entity\Db\Repository\AlerteRepository;

class DatabaseContainerFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): DatabaseContainer
    {
        $service = new DatabaseContainer();

        /** @var \UnicaenAlerte\Entity\Db\Repository\AlerteRepository $alerteRepository */
        $alerteRepository = $container->get(AlerteRepository::class);
        $service->setAlerteRepository($alerteRepository);

        return $service;
    }

}