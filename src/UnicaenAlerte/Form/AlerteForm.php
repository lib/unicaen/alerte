<?php

namespace UnicaenAlerte\Form;

use Laminas\Form\Element\Csrf;
use Laminas\Form\Element\Submit;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

class AlerteForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var \UnicaenAlerte\Entity\Db\Alerte
     */
    protected $object;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->add((new Submit('submit'))
            ->setValue("Enregister")
            ->setAttribute('class', 'btn btn-primary')
        );

        $this->add(new Csrf('csrf'));
    }

//    /**
//     * @inheritDoc
//     */
//    public function isValid(): bool
//    {
//        if (!parent::isValid()) {
//            return false;
//        }
//
//        // Des éventuels compléments associés à l'avis sélectionné peuvent faire partie d'un ensemble dans lequel
//        // l'un au moins doit être renseigné.
//        $avisTypeValeurComplems = $this->getAvisTypeValeurComplemsForAvisValeurSelectionnee();
//        $avisTypeValeurComplems = array_filter($avisTypeValeurComplems, fn(AvisTypeValeurComplem $atc) => $atc->isObligatoireUnAuMoins());
//        if ($avisTypeValeurComplems) {
//            if (!$this->atLeastOneAvisTypeValeurComplemRenseigne($avisTypeValeurComplems)) {
//                $message = count($avisTypeValeurComplems) === 1 ?
//                    "Vous devez renseigner ce complément" :
//                    "Vous devez renseigner au moins l'un de ces compléments";
//                foreach ($avisTypeValeurComplems as $avisTypeValeurComplem) {
//                    $name = $avisTypeValeurComplem->getCode();
//                    $this->get($name)->setMessages([$message]);
//                }
//
//                return false;
//            }
//        }
//
//        return true;
//    }


    /**
     * @inheritDoc
     */
    public function getInputFilterSpecification(): array
    {
        return [
            'csrf' => [
                'required' => true,
            ],
        ];
    }
}