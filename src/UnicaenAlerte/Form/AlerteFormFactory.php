<?php

namespace UnicaenAlerte\Form;

use Psr\Container\ContainerInterface;
use UnicaenAlerte\Hydrator\AlerteHydrator;

class AlerteFormFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AlerteForm
    {
        /** @var \UnicaenAlerte\Hydrator\AlerteHydrator $avisHydrator */
        $avisHydrator = $container->get('HydratorManager')->get(AlerteHydrator::class);

        $form = new AlerteForm();
        $form->setHydrator($avisHydrator);

        return $form;
    }
}