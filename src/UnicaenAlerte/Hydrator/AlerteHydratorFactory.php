<?php

namespace UnicaenAlerte\Hydrator;

use Psr\Container\ContainerInterface;
use UnicaenAlerte\Service\Db\AlerteService;

class AlerteHydratorFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AlerteHydrator
    {
        $hydrator = new AlerteHydrator();

        $avisService = $container->get(AlerteService::class);
        $hydrator->setAlerteService($avisService);

        return $hydrator;
    }
}