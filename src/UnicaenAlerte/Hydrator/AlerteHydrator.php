<?php

namespace UnicaenAlerte\Hydrator;

use Doctrine\Laminas\Hydrator\DoctrineObject;
use Doctrine\Laminas\Hydrator\Strategy\AllowRemoveByValue;
use InvalidArgumentException;
use Laminas\Hydrator\AbstractHydrator;
use UnicaenAlerte\Entity\Db\AlertePlanning;
use UnicaenAlerte\Service\Db\AlerteServiceAwareTrait;

class AlerteHydrator extends DoctrineObject//AbstractHydrator
{
    use AlerteServiceAwareTrait;

//    /**
//     * @param object|\UnicaenAlerte\Entity\Db\Alerte $object
//     * @return array
//     */
//    public function extract(object $object): array
//    {
//        $data = [];
//        $data[$object->getAvisType()->getCode()] = $object->getAvisValeur() ? $object->getAvisValeur()->getCode() : null;
//        foreach ($object->getPlannings() as $avisComplem) {
//            $data[$avisComplem->getAvisTypeValeurComplem()->getCode()] = $avisComplem->getValeur();
//        }
//
//        return $data;
//    }
//
//    /**
//     * @inheritDoc
//     */
//    public function hydrate(array $data, object $object): object
//    {
//        /** @var \UnicaenAlerte\Entity\Db\Alerte $object */
//
//        $avisType = $object->getAvisType();
//
//        $name = $avisType->getCode();
//        $avisValeur = $this->alerteService->findOneAvisValeurByCode($data[$name]);
//        $object->setAvisValeur($avisValeur);
//
//        $avisComplems = [];
//        $avisTypeValeur = $this->alerteService->findOneAvisTypeValeur($avisType, $avisValeur);
//        $avisTypeValeurComplems = $avisTypeValeur->getAvisTypeValeurComplems();
//        foreach ($avisTypeValeurComplems as $avisTypeValeurComplem) {
//            if ($this->shouldCreateAvisComplem($data, $avisTypeValeurComplem)) {
//                $name = $avisTypeValeurComplem->getCode();
//                $complemValue = $data[$name];
//
//                $avisComplem = new AlertePlanning();
//                $avisComplem
//                    ->setAlerte($object)
//                    ->setValeur($complemValue)
//                    ->setAvisTypeValeurComplem($avisTypeValeurComplem);
//
//                $avisComplems[] = $avisComplem;
//            }
//        }
//        $strategy = new AllowRemoveByValue();
//        $strategy->setObject($object)->setCollectionName('avisComplems');
//        $strategy->hydrate($avisComplems, []);
//
//        return $object;
//    }

}