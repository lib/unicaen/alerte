<?php

namespace UnicaenAlerte\Controller;

use Application\Controller\AbstractController;

class IndexController extends AbstractController
{
    public function indexAction(): array
    {
        return [];

    }
}