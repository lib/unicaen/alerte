<?php

namespace UnicaenAlerte\Service\Db;

trait AlerteServiceAwareTrait
{
    protected AlerteService $alerteService;

    public function setAlerteService(AlerteService $alerteService): void
    {
        $this->alerteService = $alerteService;
    }
}