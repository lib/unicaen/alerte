<?php

namespace UnicaenAlerte\Service\Db;

use Psr\Container\ContainerInterface;

class AlerteServiceFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AlerteService
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        $service = new AlerteService();
        $service->setObjectManager($em);

        return $service;
    }
}