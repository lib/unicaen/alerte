<?php

namespace UnicaenAlerte\Service\Db;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityRepository;
use DoctrineModule\Persistence\ProvidesObjectManager;
use RuntimeException;
use UnicaenAlerte\Entity\Db\Alerte;
use UnicaenAlerte\Entity\Db\AlertePlanning;

class AlerteService
{
    use ProvidesObjectManager;

    protected function getAlertePlanningRepository(): EntityRepository
    {
        return $this->objectManager->getRepository(AlertePlanning::class);
    }

    public function saveAlerte(Alerte $alerte): void
    {
        $this->objectManager->beginTransaction();
        try {
            $this->objectManager->persist($alerte);
            foreach ($alerte->getPlannings() as $avisComplem) {
                $this->objectManager->persist($avisComplem);
                $this->objectManager->flush($avisComplem);
            }
            $this->objectManager->flush($alerte);
            $this->objectManager->commit();
        } catch (Exception $e) {
            $this->objectManager->rollback();
            throw new RuntimeException("Erreur survenue lors de l'enregistrement, rollback!", null, $e);
        }
    }

    public function deleteAlerte(Alerte $alerte): void
    {
        $this->objectManager->beginTransaction();
        try {
            $this->objectManager->remove($alerte);
            $this->objectManager->flush($alerte);
            $this->objectManager->commit();
        } catch (Exception $e) {
            $this->objectManager->rollback();
            throw new RuntimeException("Erreur survenue lors de la suppression, rollback!", 0, $e);
        }
    }
}